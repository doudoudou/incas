/*
    ChibiOS/RT - Copyright (C) 2006,2007,2008,2009,2010,
                 2011,2012 Giovanni Di Sirio.

    This file is part of ChibiOS/RT.

    ChibiOS/RT is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    ChibiOS/RT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _BOARD_H_
#define _BOARD_H_

/*
 * Setup for St STM32437I_EVAL board.
 */

/*
 * Board identifier.
 */
#define BOARD_ST_STM32437I_EVAL
#define BOARD_NAME                  "St STM32437I_EVAL"

/*
 * Ethernet PHY type.
 */
#define BOARD_PHY_ID                MII_DP83848C_ID
#define BOARD_PHY_RMII

/*
 * Board oscillators-related settings.
 * NOTE: LSE not fitted.
 */
#if !defined(STM32_LSECLK)
#define STM32_LSECLK                0
#endif

#if !defined(STM32_HSECLK)
#define STM32_HSECLK                25000000
#endif


/*
 * Board voltages.
 * Required for performance limits calculation.
 */
#define STM32_VDD                   330

/*
 * MCU type as defined in the ST header file stm32f4xx.h.
 */
#define STM32F4XX

/*
 * IO pins assignments.
 */
#define GPIOA_BUTTON_WAKEUP         0
#define GPIOA_ETH_RMII_REF_CLK      1
#define GPIOA_ETH_RMII_MDIO         2
#define GPIOA_OTG_HS_ULPI_D0        3
#define GPIOA_PIN4                  4
#define GPIOA_OTG_HS_ULPI_CLK       5
#define GPIOA_PIN6                  6
#define GPIOA_ETH_RMII_CRS_DV       7
#define GPIOA_MCO1                  8
#define GPIOA_OTG_FS_VBUS           9
#define GPIOA_OTG_FS_ID             10
#define GPIOA_OTG_FS_DM             11
#define GPIOA_OTG_FS_DP             12
#define GPIOA_JTAG_TMS_SWDIO        13
#define GPIOA_JTAG_TCK_SWCLK        14
#define GPIOA_JTAG_TDI              15

#define GPIOB_OTG_HS_ULPI_D1        0
#define GPIOB_USB_HS_ULPI_D2        1
#define GPIOB_PIN2                  2
#define GPIOB_PIN3                  3
#define GPIOB_JTAG_TRST             4
#define GPIOB_OTG_HS_ULPI_D7        5
#define GPIOB_OTG_FS_INTN           6
#define GPIOB_PIN7                  7
#define GPIOB_OTG_FS_SCL            8
#define GPIOB_OTG_FS_SDA            9
#define GPIOB_OTG_HS_ULPI_D3        10
#define GPIOB_OTG_HS_ULPI_D4        11
#define GPIOB_OTG_HS_ULPI_D5        12
#define GPIOB_OTG_HS_ULPI_D6        13
#define GPIOB_PIN14                 14
#define GPIOB_PIN15                 15

#define GPIOC_OTG_HS_ULPI_STP       0
#define GPIOC_ETH_RMII_MDC          1
#define GPIOC_PIN2                  2
#define GPIOC_PIN3                  3
#define GPIOC_ETH_RMII_RXD0         4
#define GPIOC_ETH_RMII_RXD1         5
#define GPIOC_SmartCard_IO          6
#define GPIOC_LED4                  7
#define GPIOC_SDIO_D0               8
#define GPIOC_MCO2                  9
#define GPIOC_SDIO_D2               10
#define GPIOC_SDIO_D3               11
#define GPIOC_SDIO_CLK              12
#define GPIOC_ANTI_TAMPER           13
#define GPIOC_PIN14                 14
#define GPIOC_PIN15                 15

#define GPIOD_FSMC_D2               0
#define GPIOD_FSMC_D3               1
#define GPIOD_SDIO_CMD              2
#define GPIOD_PIN3                  3
#define GPIOD_FSMC_NOE              4
#define GPIOD_FSMC_NWE              5
#define GPIOD_PIN6                  6
#define GPIOD_FSMC_NE1              7
#define GPIOD_FSMC_D13              8
#define GPIOD_FSMC_D14              9
#define GPIOD_FSMC_D15              10
#define GPIOD_FSMC_A16              11
#define GPIOD_FSMC_A17              12
#define GPIOD_FSMC_A18              13
#define GPIOD_FSMC_D0               14
#define GPIOD_FSMC_D1               15

#define GPIOE_FSMC_NBL0             0
#define GPIOE_FSMC_NBL1             1
#define GPIOE_FSMC_A23              2
#define GPIOE_FSMC_A19              3
#define GPIOE_FSMC_A20              4
#define GPIOE_FSMC_A21              5
#define GPIOE_FSMC_A22              6
#define GPIOE_FSMC_D4               7
#define GPIOE_FSMC_D5               8
#define GPIOE_FSMC_D6               9
#define GPIOE_FSMC_D7               10
#define GPIOE_FSMC_D8               11
#define GPIOE_FSMC_D9               12
#define GPIOE_FSMC_D10              13
#define GPIOE_FSMC_D11              14
#define GPIOE_FSMC_D12              15

#define GPIOF_FSMC_A0               0
#define GPIOF_FSMC_A1               1
#define GPIOF_FSMC_A2               2
#define GPIOF_FSMC_A3               3
#define GPIOF_FSMC_A4               4
#define GPIOF_FSMC_A5               5
#define GPIOF_PIN6                  6
#define GPIOF_PIN7                  7
#define GPIOF_PIN8                  8
#define GPIOF_PIN9                  9
#define GPIOF_PIN10                 10
#define GPIOF_USB_FS_FAULT_OVER_CURRENT 11
#define GPIOF_FSMC_A6               12
#define GPIOF_FSMC_A7               13
#define GPIOF_FSMC_A8               14
#define GPIOF_FSMC_A9               15

#define GPIOG_FSMC_A10              0
#define GPIOG_FSMC_A11              1
#define GPIOG_FSMC_A12              2
#define GPIOG_FSMC_A13              3
#define GPIOG_FSMC_A14              4
#define GPIOG_FSMC_A15              5
#define GPIOG_LED1                  6
#define GPIOG_PIN7                  7
#define GPIOG_LED2                  8
#define GPIOG_FSMC_NE2              9
#define GPIOG_PIN10                 10
#define GPIOG_ETH_RMII_TXEN         11
#define GPIOG_PIN12                 12
#define GPIOG_ETH_RMII_TXD0         13
#define GPIOG_ETH_RMII_TXD1         14
#define GPIOG_BUTTON_USER           15

#define GPIOH_PIN0                  0
#define GPIOH_PIN1                  1
#define GPIOH_PIN2                  2
#define GPIOH_PIN3                  3
#define GPIOH_OTG_HS_ULPI_NXT       4
#define GPIOH_OTG_FS_PowerSwitchON  5
#define GPIOH_PIN6                  6
#define GPIOH_PIN7                  7
#define GPIOH_PIN8                  8
#define GPIOH_PIN9                  9
#define GPIOH_PIN10                 10
#define GPIOH_PIN11                 11
#define GPIOH_PIN12                 12
#define GPIOH_SDIO_CARD_DETECT      13
#define GPIOH_PIN14                 14
#define GPIOH_PIN15                 15

#define GPIOI_PIN0                  0
#define GPIOI_PIN1                  1
#define GPIOI_PIN2                  2
#define GPIOI_PIN3                  3
#define GPIOI_PIN4                  4
#define GPIOI_PIN5                  5
#define GPIOI_PIN6                  6
#define GPIOI_PIN7                  7
#define GPIOI_PIN8                  8
#define GPIOI_LED3                  9
#define GPIOI_PIN10                 10
#define GPIOI_OTG_HS_ULPI_DIR       11
#define GPIOI_PIN12                 12
#define GPIOI_PIN13                 13
#define GPIOI_PIN14                 14
#define GPIOI_PIN15                 15

/*
 * I/O ports initial setup, this configuration is established soon after reset
 * in the initialization code.
 * Please refer to the STM32 Reference Manual for details.
 */
#define PIN_MODE_INPUT(n)           (0U << ((n) * 2))
#define PIN_MODE_OUTPUT(n)          (1U << ((n) * 2))
#define PIN_MODE_ALTERNATE(n)       (2U << ((n) * 2))
#define PIN_MODE_ANALOG(n)          (3U << ((n) * 2))
#define PIN_ODR_LOW(n)              (0U << (n))
#define PIN_ODR_HIGH(n)             (1U << (n))
#define PIN_OTYPE_PUSHPULL(n)       (0U << (n))
#define PIN_OTYPE_OPENDRAIN(n)      (1U << (n))
#define PIN_OSPEED_2M(n)            (0U << ((n) * 2))
#define PIN_OSPEED_25M(n)           (1U << ((n) * 2))
#define PIN_OSPEED_50M(n)           (2U << ((n) * 2))
#define PIN_OSPEED_100M(n)          (3U << ((n) * 2))
#define PIN_PUPDR_FLOATING(n)       (0U << ((n) * 2))
#define PIN_PUPDR_PULLUP(n)         (1U << ((n) * 2))
#define PIN_PUPDR_PULLDOWN(n)       (2U << ((n) * 2))
#define PIN_AFIO_AF(n, v)           ((v##U) << ((n % 8) * 4))

/*
 * GPIOA setup:
 *
 * PA0  - BUTTON_WAKEUP             (input floating).
 * PA1  - ETH_RMII_REF_CLK          (alternate 11).
 * PA2  - ETH_RMII_MDIO             (alternate 11).
 * PA3  - OTG_HS_ULPI_D0            (alternate 10).
 * PA4  - PIN4                      (alternate 12).
 * PA5  - OTG_HS_ULPI_CLK           (alternate 10).
 * PA6  - PIN6                      (input pullup).
 * PA7  - ETH_RMII_CRS_DV           (alternate 11).
 * PA8  - MCO1                      (alternate 0).
 * PA9  - OTG_FS_VBUS               (input pulldown).
 * PA10 - OTG_FS_ID                 (input floating).
 * PA11 - OTG_FS_DM                 (alternate 10).
 * PA12 - OTG_FS_DP                 (alternate 10).
 * PA13 - JTAG_TMS_SWDIO            (alternate 0).
 * PA14 - JTAG_TCK_SWCLK            (alternate 0).
 * PA15 - JTAG_TDI                  (alternate 0).
 */
#define VAL_GPIOA_MODER             (PIN_MODE_INPUT(GPIOA_BUTTON_WAKEUP) |  \
                                     PIN_MODE_ALTERNATE(GPIOA_ETH_RMII_REF_CLK) |\
                                     PIN_MODE_ALTERNATE(GPIOA_ETH_RMII_MDIO) |\
                                     PIN_MODE_ALTERNATE(GPIOA_OTG_HS_ULPI_D0) |\
                                     PIN_MODE_ALTERNATE(GPIOA_PIN4) |       \
                                     PIN_MODE_ALTERNATE(GPIOA_OTG_HS_ULPI_CLK) |\
                                     PIN_MODE_INPUT(GPIOA_PIN6) |           \
                                     PIN_MODE_ALTERNATE(GPIOA_ETH_RMII_CRS_DV) |\
                                     PIN_MODE_ALTERNATE(GPIOA_MCO1) |       \
                                     PIN_MODE_INPUT(GPIOA_OTG_FS_VBUS) |    \
                                     PIN_MODE_INPUT(GPIOA_OTG_FS_ID) |      \
                                     PIN_MODE_ALTERNATE(GPIOA_OTG_FS_DM) |  \
                                     PIN_MODE_ALTERNATE(GPIOA_OTG_FS_DP) |  \
                                     PIN_MODE_ALTERNATE(GPIOA_JTAG_TMS_SWDIO) |\
                                     PIN_MODE_ALTERNATE(GPIOA_JTAG_TCK_SWCLK) |\
                                     PIN_MODE_ALTERNATE(GPIOA_JTAG_TDI))
#define VAL_GPIOA_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOA_BUTTON_WAKEUP) |\
                                     PIN_OTYPE_PUSHPULL(GPIOA_ETH_RMII_REF_CLK) |\
                                     PIN_OTYPE_PUSHPULL(GPIOA_ETH_RMII_MDIO) |\
                                     PIN_OTYPE_PUSHPULL(GPIOA_OTG_HS_ULPI_D0) |\
                                     PIN_OTYPE_PUSHPULL(GPIOA_PIN4) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOA_OTG_HS_ULPI_CLK) |\
                                     PIN_OTYPE_PUSHPULL(GPIOA_PIN6) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOA_ETH_RMII_CRS_DV) |\
                                     PIN_OTYPE_PUSHPULL(GPIOA_MCO1) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOA_OTG_FS_VBUS) |\
                                     PIN_OTYPE_PUSHPULL(GPIOA_OTG_FS_ID) |  \
                                     PIN_OTYPE_PUSHPULL(GPIOA_OTG_FS_DM) |  \
                                     PIN_OTYPE_PUSHPULL(GPIOA_OTG_FS_DP) |  \
                                     PIN_OTYPE_PUSHPULL(GPIOA_JTAG_TMS_SWDIO) |\
                                     PIN_OTYPE_PUSHPULL(GPIOA_JTAG_TCK_SWCLK) |\
                                     PIN_OTYPE_PUSHPULL(GPIOA_JTAG_TDI))
#define VAL_GPIOA_OSPEEDR           (PIN_OSPEED_100M(GPIOA_BUTTON_WAKEUP) | \
                                     PIN_OSPEED_100M(GPIOA_ETH_RMII_REF_CLK) |\
                                     PIN_OSPEED_100M(GPIOA_ETH_RMII_MDIO) | \
                                     PIN_OSPEED_100M(GPIOA_OTG_HS_ULPI_D0) |\
                                     PIN_OSPEED_100M(GPIOA_PIN4) |          \
                                     PIN_OSPEED_100M(GPIOA_OTG_HS_ULPI_CLK) |\
                                     PIN_OSPEED_100M(GPIOA_PIN6) |          \
                                     PIN_OSPEED_100M(GPIOA_ETH_RMII_CRS_DV) |\
                                     PIN_OSPEED_100M(GPIOA_MCO1) |          \
                                     PIN_OSPEED_100M(GPIOA_OTG_FS_VBUS) |   \
                                     PIN_OSPEED_100M(GPIOA_OTG_FS_ID) |     \
                                     PIN_OSPEED_100M(GPIOA_OTG_FS_DM) |     \
                                     PIN_OSPEED_100M(GPIOA_OTG_FS_DP) |     \
                                     PIN_OSPEED_100M(GPIOA_JTAG_TMS_SWDIO) |\
                                     PIN_OSPEED_100M(GPIOA_JTAG_TCK_SWCLK) |\
                                     PIN_OSPEED_100M(GPIOA_JTAG_TDI))
#define VAL_GPIOA_PUPDR             (PIN_PUPDR_FLOATING(GPIOA_BUTTON_WAKEUP) |\
                                     PIN_PUPDR_FLOATING(GPIOA_ETH_RMII_REF_CLK) |\
                                     PIN_PUPDR_FLOATING(GPIOA_ETH_RMII_MDIO) |\
                                     PIN_PUPDR_FLOATING(GPIOA_OTG_HS_ULPI_D0) |\
                                     PIN_PUPDR_PULLUP(GPIOA_PIN4) |         \
                                     PIN_PUPDR_PULLUP(GPIOA_OTG_HS_ULPI_CLK) |\
                                     PIN_PUPDR_PULLUP(GPIOA_PIN6) |         \
                                     PIN_PUPDR_FLOATING(GPIOA_ETH_RMII_CRS_DV) |\
                                     PIN_PUPDR_FLOATING(GPIOA_MCO1) |       \
                                     PIN_PUPDR_PULLDOWN(GPIOA_OTG_FS_VBUS) |\
                                     PIN_PUPDR_FLOATING(GPIOA_OTG_FS_ID) |  \
                                     PIN_PUPDR_FLOATING(GPIOA_OTG_FS_DM) |  \
                                     PIN_PUPDR_FLOATING(GPIOA_OTG_FS_DP) |  \
                                     PIN_PUPDR_FLOATING(GPIOA_JTAG_TMS_SWDIO) |\
                                     PIN_PUPDR_PULLDOWN(GPIOA_JTAG_TCK_SWCLK) |\
                                     PIN_PUPDR_FLOATING(GPIOA_JTAG_TDI))
#define VAL_GPIOA_ODR               (PIN_ODR_HIGH(GPIOA_BUTTON_WAKEUP) |    \
                                     PIN_ODR_HIGH(GPIOA_ETH_RMII_REF_CLK) | \
                                     PIN_ODR_HIGH(GPIOA_ETH_RMII_MDIO) |    \
                                     PIN_ODR_HIGH(GPIOA_OTG_HS_ULPI_D0) |   \
                                     PIN_ODR_HIGH(GPIOA_PIN4) |             \
                                     PIN_ODR_HIGH(GPIOA_OTG_HS_ULPI_CLK) |  \
                                     PIN_ODR_HIGH(GPIOA_PIN6) |             \
                                     PIN_ODR_HIGH(GPIOA_ETH_RMII_CRS_DV) |  \
                                     PIN_ODR_HIGH(GPIOA_MCO1) |             \
                                     PIN_ODR_HIGH(GPIOA_OTG_FS_VBUS) |      \
                                     PIN_ODR_HIGH(GPIOA_OTG_FS_ID) |        \
                                     PIN_ODR_HIGH(GPIOA_OTG_FS_DM) |        \
                                     PIN_ODR_HIGH(GPIOA_OTG_FS_DP) |        \
                                     PIN_ODR_HIGH(GPIOA_JTAG_TMS_SWDIO) |   \
                                     PIN_ODR_HIGH(GPIOA_JTAG_TCK_SWCLK) |   \
                                     PIN_ODR_HIGH(GPIOA_JTAG_TDI))
#define VAL_GPIOA_AFRL              (PIN_AFIO_AF(GPIOA_BUTTON_WAKEUP, 0) |  \
                                     PIN_AFIO_AF(GPIOA_ETH_RMII_REF_CLK, 11) |\
                                     PIN_AFIO_AF(GPIOA_ETH_RMII_MDIO, 11) | \
                                     PIN_AFIO_AF(GPIOA_OTG_HS_ULPI_D0, 10) |\
                                     PIN_AFIO_AF(GPIOA_PIN4, 12) |          \
                                     PIN_AFIO_AF(GPIOA_OTG_HS_ULPI_CLK, 10) |\
                                     PIN_AFIO_AF(GPIOA_PIN6, 0) |           \
                                     PIN_AFIO_AF(GPIOA_ETH_RMII_CRS_DV, 11))
#define VAL_GPIOA_AFRH              (PIN_AFIO_AF(GPIOA_MCO1, 0) |           \
                                     PIN_AFIO_AF(GPIOA_OTG_FS_VBUS, 0) |    \
                                     PIN_AFIO_AF(GPIOA_OTG_FS_ID, 0) |      \
                                     PIN_AFIO_AF(GPIOA_OTG_FS_DM, 10) |     \
                                     PIN_AFIO_AF(GPIOA_OTG_FS_DP, 10) |     \
                                     PIN_AFIO_AF(GPIOA_JTAG_TMS_SWDIO, 0) | \
                                     PIN_AFIO_AF(GPIOA_JTAG_TCK_SWCLK, 0) | \
                                     PIN_AFIO_AF(GPIOA_JTAG_TDI, 0))

/*
 * GPIOB setup:
 *
 * PB0  - OTG_HS_ULPI_D1            (alternate 10).
 * PB1  - USB_HS_ULPI_D2            (alternate 10).
 * PB2  - PIN2                      (input floating).
 * PB3  - PIN3                      (alternate 0).
 * PB4  - JTAG_TRST                 (input floating).
 * PB5  - OTG_HS_ULPI_D7            (alternate 10).
 * PB6  - OTG_FS_INTN               (input pullup).
 * PB7  - PIN7                      (input pullup).
 * PB8  - OTG_FS_SCL                (alternate 4).
 * PB9  - OTG_FS_SDA                (alternate 4).
 * PB10 - OTG_HS_ULPI_D3            (alternate 10).
 * PB11 - OTG_HS_ULPI_D4            (alternate 10).
 * PB12 - OTG_HS_ULPI_D5            (alternate 10).
 * PB13 - OTG_HS_ULPI_D6            (alternate 10).
 * PB14 - PIN14                     (alternate 12).
 * PB15 - PIN15                     (alternate 12).
 */
#define VAL_GPIOB_MODER             (PIN_MODE_ALTERNATE(GPIOB_OTG_HS_ULPI_D1) |\
                                     PIN_MODE_ALTERNATE(GPIOB_USB_HS_ULPI_D2) |\
                                     PIN_MODE_INPUT(GPIOB_PIN2) |           \
                                     PIN_MODE_ALTERNATE(GPIOB_PIN3) |       \
                                     PIN_MODE_INPUT(GPIOB_JTAG_TRST) |      \
                                     PIN_MODE_ALTERNATE(GPIOB_OTG_HS_ULPI_D7) |\
                                     PIN_MODE_INPUT(GPIOB_OTG_FS_INTN) |    \
                                     PIN_MODE_INPUT(GPIOB_PIN7) |           \
                                     PIN_MODE_ALTERNATE(GPIOB_OTG_FS_SCL) | \
                                     PIN_MODE_ALTERNATE(GPIOB_OTG_FS_SDA) | \
                                     PIN_MODE_ALTERNATE(GPIOB_OTG_HS_ULPI_D3) |\
                                     PIN_MODE_ALTERNATE(GPIOB_OTG_HS_ULPI_D4) |\
                                     PIN_MODE_ALTERNATE(GPIOB_OTG_HS_ULPI_D5) |\
                                     PIN_MODE_ALTERNATE(GPIOB_OTG_HS_ULPI_D6) |\
                                     PIN_MODE_ALTERNATE(GPIOB_PIN14) |      \
                                     PIN_MODE_ALTERNATE(GPIOB_PIN15))
#define VAL_GPIOB_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOB_OTG_HS_ULPI_D1) |\
                                     PIN_OTYPE_PUSHPULL(GPIOB_USB_HS_ULPI_D2) |\
                                     PIN_OTYPE_PUSHPULL(GPIOB_PIN2) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOB_PIN3) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOB_JTAG_TRST) |  \
                                     PIN_OTYPE_PUSHPULL(GPIOB_OTG_HS_ULPI_D7) |\
                                     PIN_OTYPE_PUSHPULL(GPIOB_OTG_FS_INTN) |\
                                     PIN_OTYPE_PUSHPULL(GPIOB_PIN7) |       \
                                     PIN_OTYPE_OPENDRAIN(GPIOB_OTG_FS_SCL) |\
                                     PIN_OTYPE_OPENDRAIN(GPIOB_OTG_FS_SDA) |\
                                     PIN_OTYPE_PUSHPULL(GPIOB_OTG_HS_ULPI_D3) |\
                                     PIN_OTYPE_PUSHPULL(GPIOB_OTG_HS_ULPI_D4) |\
                                     PIN_OTYPE_PUSHPULL(GPIOB_OTG_HS_ULPI_D5) |\
                                     PIN_OTYPE_PUSHPULL(GPIOB_OTG_HS_ULPI_D6) |\
                                     PIN_OTYPE_PUSHPULL(GPIOB_PIN14) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOB_PIN15))
#define VAL_GPIOB_OSPEEDR           (PIN_OSPEED_100M(GPIOB_OTG_HS_ULPI_D1) |\
                                     PIN_OSPEED_100M(GPIOB_USB_HS_ULPI_D2) |\
                                     PIN_OSPEED_100M(GPIOB_PIN2) |          \
                                     PIN_OSPEED_100M(GPIOB_PIN3) |          \
                                     PIN_OSPEED_100M(GPIOB_JTAG_TRST) |     \
                                     PIN_OSPEED_100M(GPIOB_OTG_HS_ULPI_D7) |\
                                     PIN_OSPEED_100M(GPIOB_OTG_FS_INTN) |   \
                                     PIN_OSPEED_100M(GPIOB_PIN7) |          \
                                     PIN_OSPEED_100M(GPIOB_OTG_FS_SCL) |    \
                                     PIN_OSPEED_100M(GPIOB_OTG_FS_SDA) |    \
                                     PIN_OSPEED_100M(GPIOB_OTG_HS_ULPI_D3) |\
                                     PIN_OSPEED_100M(GPIOB_OTG_HS_ULPI_D4) |\
                                     PIN_OSPEED_100M(GPIOB_OTG_HS_ULPI_D5) |\
                                     PIN_OSPEED_100M(GPIOB_OTG_HS_ULPI_D6) |\
                                     PIN_OSPEED_100M(GPIOB_PIN14) |         \
                                     PIN_OSPEED_100M(GPIOB_PIN15))
#define VAL_GPIOB_PUPDR             (PIN_PUPDR_FLOATING(GPIOB_OTG_HS_ULPI_D1) |\
                                     PIN_PUPDR_FLOATING(GPIOB_USB_HS_ULPI_D2) |\
                                     PIN_PUPDR_FLOATING(GPIOB_PIN2) |       \
                                     PIN_PUPDR_FLOATING(GPIOB_PIN3) |       \
                                     PIN_PUPDR_FLOATING(GPIOB_JTAG_TRST) |  \
                                     PIN_PUPDR_PULLUP(GPIOB_OTG_HS_ULPI_D7) |\
                                     PIN_PUPDR_PULLUP(GPIOB_OTG_FS_INTN) |  \
                                     PIN_PUPDR_PULLUP(GPIOB_PIN7) |         \
                                     PIN_PUPDR_FLOATING(GPIOB_OTG_FS_SCL) | \
                                     PIN_PUPDR_FLOATING(GPIOB_OTG_FS_SDA) | \
                                     PIN_PUPDR_FLOATING(GPIOB_OTG_HS_ULPI_D3) |\
                                     PIN_PUPDR_FLOATING(GPIOB_OTG_HS_ULPI_D4) |\
                                     PIN_PUPDR_FLOATING(GPIOB_OTG_HS_ULPI_D5) |\
                                     PIN_PUPDR_FLOATING(GPIOB_OTG_HS_ULPI_D6) |\
                                     PIN_PUPDR_FLOATING(GPIOB_PIN14) |      \
                                     PIN_PUPDR_FLOATING(GPIOB_PIN15))
#define VAL_GPIOB_ODR               (PIN_ODR_HIGH(GPIOB_OTG_HS_ULPI_D1) |   \
                                     PIN_ODR_HIGH(GPIOB_USB_HS_ULPI_D2) |   \
                                     PIN_ODR_HIGH(GPIOB_PIN2) |             \
                                     PIN_ODR_HIGH(GPIOB_PIN3) |             \
                                     PIN_ODR_HIGH(GPIOB_JTAG_TRST) |        \
                                     PIN_ODR_HIGH(GPIOB_OTG_HS_ULPI_D7) |   \
                                     PIN_ODR_HIGH(GPIOB_OTG_FS_INTN) |      \
                                     PIN_ODR_HIGH(GPIOB_PIN7) |             \
                                     PIN_ODR_HIGH(GPIOB_OTG_FS_SCL) |       \
                                     PIN_ODR_HIGH(GPIOB_OTG_FS_SDA) |       \
                                     PIN_ODR_HIGH(GPIOB_OTG_HS_ULPI_D3) |   \
                                     PIN_ODR_HIGH(GPIOB_OTG_HS_ULPI_D4) |   \
                                     PIN_ODR_HIGH(GPIOB_OTG_HS_ULPI_D5) |   \
                                     PIN_ODR_HIGH(GPIOB_OTG_HS_ULPI_D6) |   \
                                     PIN_ODR_HIGH(GPIOB_PIN14) |            \
                                     PIN_ODR_HIGH(GPIOB_PIN15))
#define VAL_GPIOB_AFRL              (PIN_AFIO_AF(GPIOB_OTG_HS_ULPI_D1, 10) |\
                                     PIN_AFIO_AF(GPIOB_USB_HS_ULPI_D2, 10) |\
                                     PIN_AFIO_AF(GPIOB_PIN2, 0) |           \
                                     PIN_AFIO_AF(GPIOB_PIN3, 0) |           \
                                     PIN_AFIO_AF(GPIOB_JTAG_TRST, 0) |      \
                                     PIN_AFIO_AF(GPIOB_OTG_HS_ULPI_D7, 10) |\
                                     PIN_AFIO_AF(GPIOB_OTG_FS_INTN, 0) |    \
                                     PIN_AFIO_AF(GPIOB_PIN7, 0))
#define VAL_GPIOB_AFRH              (PIN_AFIO_AF(GPIOB_OTG_FS_SCL, 4) |     \
                                     PIN_AFIO_AF(GPIOB_OTG_FS_SDA, 4) |     \
                                     PIN_AFIO_AF(GPIOB_OTG_HS_ULPI_D3, 10) |\
                                     PIN_AFIO_AF(GPIOB_OTG_HS_ULPI_D4, 10) |\
                                     PIN_AFIO_AF(GPIOB_OTG_HS_ULPI_D5, 10) |\
                                     PIN_AFIO_AF(GPIOB_OTG_HS_ULPI_D6, 10) |\
                                     PIN_AFIO_AF(GPIOB_PIN14, 12) |         \
                                     PIN_AFIO_AF(GPIOB_PIN15, 12))

/*
 * GPIOC setup:
 *
 * PC0  - OTG_HS_ULPI_STP           (alternate 10).
 * PC1  - ETH_RMII_MDC              (alternate 11).
 * PC2  - PIN2                      (alternate 5).
 * PC3  - PIN3                      (alternate 5).
 * PC4  - ETH_RMII_RXD0             (alternate 11).
 * PC5  - ETH_RMII_RXD1             (alternate 11).
 * PC6  - SmartCard_IO              (alternate 8).
 * PC7  - LED4                      (output pushpull maximum).
 * PC8  - SDIO_D0                   (alternate 12).
 * PC9  - MCO2                      (alternate 0).
 * PC10 - SDIO_D2                   (alternate 12).
 * PC11 - SDIO_D3                   (alternate 12).
 * PC12 - SDIO_CLK                  (alternate 12).
 * PC13 - ANTI_TAMPER               (output pushpull maximum).
 * PC14 - PIN14                     (input floating).
 * PC15 - PIN15                     (input floating).
 */
#define VAL_GPIOC_MODER             (PIN_MODE_ALTERNATE(GPIOC_OTG_HS_ULPI_STP) |\
                                     PIN_MODE_ALTERNATE(GPIOC_ETH_RMII_MDC) |\
                                     PIN_MODE_ALTERNATE(GPIOC_PIN2) |       \
                                     PIN_MODE_ALTERNATE(GPIOC_PIN3) |       \
                                     PIN_MODE_ALTERNATE(GPIOC_ETH_RMII_RXD0) |\
                                     PIN_MODE_ALTERNATE(GPIOC_ETH_RMII_RXD1) |\
                                     PIN_MODE_ALTERNATE(GPIOC_SmartCard_IO) |\
                                     PIN_MODE_OUTPUT(GPIOC_LED4) |          \
                                     PIN_MODE_ALTERNATE(GPIOC_SDIO_D0) |    \
                                     PIN_MODE_ALTERNATE(GPIOC_MCO2) |       \
                                     PIN_MODE_ALTERNATE(GPIOC_SDIO_D2) |    \
                                     PIN_MODE_ALTERNATE(GPIOC_SDIO_D3) |    \
                                     PIN_MODE_ALTERNATE(GPIOC_SDIO_CLK) |   \
                                     PIN_MODE_OUTPUT(GPIOC_ANTI_TAMPER) |   \
                                     PIN_MODE_INPUT(GPIOC_PIN14) |          \
                                     PIN_MODE_INPUT(GPIOC_PIN15))
#define VAL_GPIOC_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOC_OTG_HS_ULPI_STP) |\
                                     PIN_OTYPE_PUSHPULL(GPIOC_ETH_RMII_MDC) |\
                                     PIN_OTYPE_PUSHPULL(GPIOC_PIN2) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOC_PIN3) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOC_ETH_RMII_RXD0) |\
                                     PIN_OTYPE_PUSHPULL(GPIOC_ETH_RMII_RXD1) |\
                                     PIN_OTYPE_PUSHPULL(GPIOC_SmartCard_IO) |\
                                     PIN_OTYPE_PUSHPULL(GPIOC_LED4) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOC_SDIO_D0) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOC_MCO2) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOC_SDIO_D2) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOC_SDIO_D3) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOC_SDIO_CLK) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOC_ANTI_TAMPER) |\
                                     PIN_OTYPE_PUSHPULL(GPIOC_PIN14) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOC_PIN15))
#define VAL_GPIOC_OSPEEDR           (PIN_OSPEED_100M(GPIOC_OTG_HS_ULPI_STP) |\
                                     PIN_OSPEED_100M(GPIOC_ETH_RMII_MDC) |  \
                                     PIN_OSPEED_100M(GPIOC_PIN2) |          \
                                     PIN_OSPEED_100M(GPIOC_PIN3) |          \
                                     PIN_OSPEED_100M(GPIOC_ETH_RMII_RXD0) | \
                                     PIN_OSPEED_100M(GPIOC_ETH_RMII_RXD1) | \
                                     PIN_OSPEED_100M(GPIOC_SmartCard_IO) |  \
                                     PIN_OSPEED_100M(GPIOC_LED4) |          \
                                     PIN_OSPEED_100M(GPIOC_SDIO_D0) |       \
                                     PIN_OSPEED_100M(GPIOC_MCO2) |          \
                                     PIN_OSPEED_100M(GPIOC_SDIO_D2) |       \
                                     PIN_OSPEED_100M(GPIOC_SDIO_D3) |       \
                                     PIN_OSPEED_100M(GPIOC_SDIO_CLK) |      \
                                     PIN_OSPEED_100M(GPIOC_ANTI_TAMPER) |   \
                                     PIN_OSPEED_100M(GPIOC_PIN14) |         \
                                     PIN_OSPEED_100M(GPIOC_PIN15))
#define VAL_GPIOC_PUPDR             (PIN_PUPDR_FLOATING(GPIOC_OTG_HS_ULPI_STP) |\
                                     PIN_PUPDR_FLOATING(GPIOC_ETH_RMII_MDC) |\
                                     PIN_PUPDR_FLOATING(GPIOC_PIN2) |       \
                                     PIN_PUPDR_FLOATING(GPIOC_PIN3) |       \
                                     PIN_PUPDR_FLOATING(GPIOC_ETH_RMII_RXD0) |\
                                     PIN_PUPDR_FLOATING(GPIOC_ETH_RMII_RXD1) |\
                                     PIN_PUPDR_FLOATING(GPIOC_SmartCard_IO) |\
                                     PIN_PUPDR_PULLUP(GPIOC_LED4) |         \
                                     PIN_PUPDR_FLOATING(GPIOC_SDIO_D0) |    \
                                     PIN_PUPDR_FLOATING(GPIOC_MCO2) |       \
                                     PIN_PUPDR_FLOATING(GPIOC_SDIO_D2) |    \
                                     PIN_PUPDR_FLOATING(GPIOC_SDIO_D3) |    \
                                     PIN_PUPDR_FLOATING(GPIOC_SDIO_CLK) |   \
                                     PIN_PUPDR_FLOATING(GPIOC_ANTI_TAMPER) |\
                                     PIN_PUPDR_FLOATING(GPIOC_PIN14) |      \
                                     PIN_PUPDR_FLOATING(GPIOC_PIN15))
#define VAL_GPIOC_ODR               (PIN_ODR_HIGH(GPIOC_OTG_HS_ULPI_STP) |  \
                                     PIN_ODR_HIGH(GPIOC_ETH_RMII_MDC) |     \
                                     PIN_ODR_HIGH(GPIOC_PIN2) |             \
                                     PIN_ODR_HIGH(GPIOC_PIN3) |             \
                                     PIN_ODR_HIGH(GPIOC_ETH_RMII_RXD0) |    \
                                     PIN_ODR_HIGH(GPIOC_ETH_RMII_RXD1) |    \
                                     PIN_ODR_HIGH(GPIOC_SmartCard_IO) |     \
                                     PIN_ODR_LOW(GPIOC_LED4) |              \
                                     PIN_ODR_HIGH(GPIOC_SDIO_D0) |          \
                                     PIN_ODR_HIGH(GPIOC_MCO2) |             \
                                     PIN_ODR_HIGH(GPIOC_SDIO_D2) |          \
                                     PIN_ODR_HIGH(GPIOC_SDIO_D3) |          \
                                     PIN_ODR_HIGH(GPIOC_SDIO_CLK) |         \
                                     PIN_ODR_HIGH(GPIOC_ANTI_TAMPER) |      \
                                     PIN_ODR_HIGH(GPIOC_PIN14) |            \
                                     PIN_ODR_HIGH(GPIOC_PIN15))
#define VAL_GPIOC_AFRL              (PIN_AFIO_AF(GPIOC_OTG_HS_ULPI_STP, 10) |\
                                     PIN_AFIO_AF(GPIOC_ETH_RMII_MDC, 11) |  \
                                     PIN_AFIO_AF(GPIOC_PIN2, 5) |           \
                                     PIN_AFIO_AF(GPIOC_PIN3, 5) |           \
                                     PIN_AFIO_AF(GPIOC_ETH_RMII_RXD0, 11) | \
                                     PIN_AFIO_AF(GPIOC_ETH_RMII_RXD1, 11) | \
                                     PIN_AFIO_AF(GPIOC_SmartCard_IO, 8) |   \
                                     PIN_AFIO_AF(GPIOC_LED4, 0))
#define VAL_GPIOC_AFRH              (PIN_AFIO_AF(GPIOC_SDIO_D0, 12) |       \
                                     PIN_AFIO_AF(GPIOC_MCO2, 0) |           \
                                     PIN_AFIO_AF(GPIOC_SDIO_D2, 12) |       \
                                     PIN_AFIO_AF(GPIOC_SDIO_D3, 12) |       \
                                     PIN_AFIO_AF(GPIOC_SDIO_CLK, 12) |      \
                                     PIN_AFIO_AF(GPIOC_ANTI_TAMPER, 0) |    \
                                     PIN_AFIO_AF(GPIOC_PIN14, 0) |          \
                                     PIN_AFIO_AF(GPIOC_PIN15, 0))

/*
 * GPIOD setup:
 *
 * PD0  - FSMC_D2                   (alternate 12).
 * PD1  - FSMC_D3                   (alternate 12).
 * PD2  - SDIO_CMD                  (alternate 12).
 * PD3  - PIN3                      (input pullup).
 * PD4  - FSMC_NOE                  (alternate 12).
 * PD5  - FSMC_NWE                  (alternate 12).
 * PD6  - PIN6                      (input pullup).
 * PD7  - FSMC_NE1                  (alternate 12).
 * PD8  - FSMC_D13                  (alternate 12).
 * PD9  - FSMC_D14                  (alternate 12).
 * PD10 - FSMC_D15                  (alternate 12).
 * PD11 - FSMC_A16                  (alternate 12).
 * PD12 - FSMC_A17                  (alternate 12).
 * PD13 - FSMC_A18                  (alternate 12).
 * PD14 - FSMC_D0                   (alternate 12).
 * PD15 - FSMC_D1                   (alternate 12).
 */
#define VAL_GPIOD_MODER             (PIN_MODE_ALTERNATE(GPIOD_FSMC_D2) |    \
                                     PIN_MODE_ALTERNATE(GPIOD_FSMC_D3) |    \
                                     PIN_MODE_ALTERNATE(GPIOD_SDIO_CMD) |   \
                                     PIN_MODE_INPUT(GPIOD_PIN3) |           \
                                     PIN_MODE_ALTERNATE(GPIOD_FSMC_NOE) |   \
                                     PIN_MODE_ALTERNATE(GPIOD_FSMC_NWE) |   \
                                     PIN_MODE_INPUT(GPIOD_PIN6) |           \
                                     PIN_MODE_ALTERNATE(GPIOD_FSMC_NE1) |   \
                                     PIN_MODE_ALTERNATE(GPIOD_FSMC_D13) |   \
                                     PIN_MODE_ALTERNATE(GPIOD_FSMC_D14) |   \
                                     PIN_MODE_ALTERNATE(GPIOD_FSMC_D15) |   \
                                     PIN_MODE_ALTERNATE(GPIOD_FSMC_A16) |   \
                                     PIN_MODE_ALTERNATE(GPIOD_FSMC_A17) |   \
                                     PIN_MODE_ALTERNATE(GPIOD_FSMC_A18) |   \
                                     PIN_MODE_ALTERNATE(GPIOD_FSMC_D0) |    \
                                     PIN_MODE_ALTERNATE(GPIOD_FSMC_D1))
#define VAL_GPIOD_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOD_FSMC_D2) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOD_FSMC_D3) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOD_SDIO_CMD) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOD_PIN3) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOD_FSMC_NOE) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOD_FSMC_NWE) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOD_PIN6) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOD_FSMC_NE1) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOD_FSMC_D13) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOD_FSMC_D14) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOD_FSMC_D15) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOD_FSMC_A16) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOD_FSMC_A17) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOD_FSMC_A18) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOD_FSMC_D0) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOD_FSMC_D1))
#define VAL_GPIOD_OSPEEDR           (PIN_OSPEED_100M(GPIOD_FSMC_D2) |       \
                                     PIN_OSPEED_100M(GPIOD_FSMC_D3) |       \
                                     PIN_OSPEED_100M(GPIOD_SDIO_CMD) |      \
                                     PIN_OSPEED_100M(GPIOD_PIN3) |          \
                                     PIN_OSPEED_100M(GPIOD_FSMC_NOE) |      \
                                     PIN_OSPEED_100M(GPIOD_FSMC_NWE) |      \
                                     PIN_OSPEED_100M(GPIOD_PIN6) |          \
                                     PIN_OSPEED_100M(GPIOD_FSMC_NE1) |      \
                                     PIN_OSPEED_100M(GPIOD_FSMC_D13) |      \
                                     PIN_OSPEED_100M(GPIOD_FSMC_D14) |      \
                                     PIN_OSPEED_100M(GPIOD_FSMC_D15) |      \
                                     PIN_OSPEED_100M(GPIOD_FSMC_A16) |      \
                                     PIN_OSPEED_100M(GPIOD_FSMC_A17) |      \
                                     PIN_OSPEED_100M(GPIOD_FSMC_A18) |      \
                                     PIN_OSPEED_100M(GPIOD_FSMC_D0) |       \
                                     PIN_OSPEED_100M(GPIOD_FSMC_D1))
#define VAL_GPIOD_PUPDR             (PIN_PUPDR_FLOATING(GPIOD_FSMC_D2) |    \
                                     PIN_PUPDR_FLOATING(GPIOD_FSMC_D3) |    \
                                     PIN_PUPDR_FLOATING(GPIOD_SDIO_CMD) |   \
                                     PIN_PUPDR_PULLUP(GPIOD_PIN3) |         \
                                     PIN_PUPDR_FLOATING(GPIOD_FSMC_NOE) |   \
                                     PIN_PUPDR_FLOATING(GPIOD_FSMC_NWE) |   \
                                     PIN_PUPDR_PULLUP(GPIOD_PIN6) |         \
                                     PIN_PUPDR_FLOATING(GPIOD_FSMC_NE1) |   \
                                     PIN_PUPDR_FLOATING(GPIOD_FSMC_D13) |   \
                                     PIN_PUPDR_FLOATING(GPIOD_FSMC_D14) |   \
                                     PIN_PUPDR_FLOATING(GPIOD_FSMC_D15) |   \
                                     PIN_PUPDR_FLOATING(GPIOD_FSMC_A16) |   \
                                     PIN_PUPDR_FLOATING(GPIOD_FSMC_A17) |   \
                                     PIN_PUPDR_FLOATING(GPIOD_FSMC_A18) |   \
                                     PIN_PUPDR_FLOATING(GPIOD_FSMC_D0) |    \
                                     PIN_PUPDR_FLOATING(GPIOD_FSMC_D1))
#define VAL_GPIOD_ODR               (PIN_ODR_HIGH(GPIOD_FSMC_D2) |          \
                                     PIN_ODR_HIGH(GPIOD_FSMC_D3) |          \
                                     PIN_ODR_HIGH(GPIOD_SDIO_CMD) |         \
                                     PIN_ODR_HIGH(GPIOD_PIN3) |             \
                                     PIN_ODR_HIGH(GPIOD_FSMC_NOE) |         \
                                     PIN_ODR_HIGH(GPIOD_FSMC_NWE) |         \
                                     PIN_ODR_HIGH(GPIOD_PIN6) |             \
                                     PIN_ODR_HIGH(GPIOD_FSMC_NE1) |         \
                                     PIN_ODR_HIGH(GPIOD_FSMC_D13) |         \
                                     PIN_ODR_HIGH(GPIOD_FSMC_D14) |         \
                                     PIN_ODR_HIGH(GPIOD_FSMC_D15) |         \
                                     PIN_ODR_HIGH(GPIOD_FSMC_A16) |         \
                                     PIN_ODR_HIGH(GPIOD_FSMC_A17) |         \
                                     PIN_ODR_HIGH(GPIOD_FSMC_A18) |         \
                                     PIN_ODR_HIGH(GPIOD_FSMC_D0) |          \
                                     PIN_ODR_HIGH(GPIOD_FSMC_D1))
#define VAL_GPIOD_AFRL              (PIN_AFIO_AF(GPIOD_FSMC_D2, 12) |       \
                                     PIN_AFIO_AF(GPIOD_FSMC_D3, 12) |       \
                                     PIN_AFIO_AF(GPIOD_SDIO_CMD, 12) |      \
                                     PIN_AFIO_AF(GPIOD_PIN3, 0) |           \
                                     PIN_AFIO_AF(GPIOD_FSMC_NOE, 12) |      \
                                     PIN_AFIO_AF(GPIOD_FSMC_NWE, 12) |      \
                                     PIN_AFIO_AF(GPIOD_PIN6, 0) |           \
                                     PIN_AFIO_AF(GPIOD_FSMC_NE1, 12))
#define VAL_GPIOD_AFRH              (PIN_AFIO_AF(GPIOD_FSMC_D13, 12) |      \
                                     PIN_AFIO_AF(GPIOD_FSMC_D14, 12) |      \
                                     PIN_AFIO_AF(GPIOD_FSMC_D15, 12) |      \
                                     PIN_AFIO_AF(GPIOD_FSMC_A16, 12) |      \
                                     PIN_AFIO_AF(GPIOD_FSMC_A17, 12) |      \
                                     PIN_AFIO_AF(GPIOD_FSMC_A18, 12) |      \
                                     PIN_AFIO_AF(GPIOD_FSMC_D0, 12) |       \
                                     PIN_AFIO_AF(GPIOD_FSMC_D1, 12))

/*
 * GPIOE setup:
 *
 * PE0  - FSMC_NBL0                 (alternate 12).
 * PE1  - FSMC_NBL1                 (alternate 12).
 * PE2  - FSMC_A23                  (alternate 12).
 * PE3  - FSMC_A19                  (alternate 12).
 * PE4  - FSMC_A20                  (alternate 12).
 * PE5  - FSMC_A21                  (alternate 12).
 * PE6  - FSMC_A22                  (alternate 12).
 * PE7  - FSMC_D4                   (alternate 12).
 * PE8  - FSMC_D5                   (alternate 12).
 * PE9  - FSMC_D6                   (alternate 12).
 * PE10 - FSMC_D7                   (alternate 12).
 * PE11 - FSMC_D8                   (alternate 12).
 * PE12 - FSMC_D9                   (alternate 12).
 * PE13 - FSMC_D10                  (alternate 12).
 * PE14 - FSMC_D11                  (alternate 12).
 * PE15 - FSMC_D12                  (alternate 12).
 */
#define VAL_GPIOE_MODER             (PIN_MODE_ALTERNATE(GPIOE_FSMC_NBL0) |  \
                                     PIN_MODE_ALTERNATE(GPIOE_FSMC_NBL1) |  \
                                     PIN_MODE_ALTERNATE(GPIOE_FSMC_A23) |   \
                                     PIN_MODE_ALTERNATE(GPIOE_FSMC_A19) |   \
                                     PIN_MODE_ALTERNATE(GPIOE_FSMC_A20) |   \
                                     PIN_MODE_ALTERNATE(GPIOE_FSMC_A21) |   \
                                     PIN_MODE_ALTERNATE(GPIOE_FSMC_A22) |   \
                                     PIN_MODE_ALTERNATE(GPIOE_FSMC_D4) |    \
                                     PIN_MODE_ALTERNATE(GPIOE_FSMC_D5) |    \
                                     PIN_MODE_ALTERNATE(GPIOE_FSMC_D6) |    \
                                     PIN_MODE_ALTERNATE(GPIOE_FSMC_D7) |    \
                                     PIN_MODE_ALTERNATE(GPIOE_FSMC_D8) |    \
                                     PIN_MODE_ALTERNATE(GPIOE_FSMC_D9) |    \
                                     PIN_MODE_ALTERNATE(GPIOE_FSMC_D10) |   \
                                     PIN_MODE_ALTERNATE(GPIOE_FSMC_D11) |   \
                                     PIN_MODE_ALTERNATE(GPIOE_FSMC_D12))
#define VAL_GPIOE_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOE_FSMC_NBL0) |  \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FSMC_NBL1) |  \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FSMC_A23) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FSMC_A19) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FSMC_A20) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FSMC_A21) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FSMC_A22) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FSMC_D4) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FSMC_D5) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FSMC_D6) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FSMC_D7) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FSMC_D8) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FSMC_D9) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FSMC_D10) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FSMC_D11) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOE_FSMC_D12))
#define VAL_GPIOE_OSPEEDR           (PIN_OSPEED_100M(GPIOE_FSMC_NBL0) |     \
                                     PIN_OSPEED_100M(GPIOE_FSMC_NBL1) |     \
                                     PIN_OSPEED_100M(GPIOE_FSMC_A23) |      \
                                     PIN_OSPEED_100M(GPIOE_FSMC_A19) |      \
                                     PIN_OSPEED_100M(GPIOE_FSMC_A20) |      \
                                     PIN_OSPEED_100M(GPIOE_FSMC_A21) |      \
                                     PIN_OSPEED_100M(GPIOE_FSMC_A22) |      \
                                     PIN_OSPEED_100M(GPIOE_FSMC_D4) |       \
                                     PIN_OSPEED_100M(GPIOE_FSMC_D5) |       \
                                     PIN_OSPEED_100M(GPIOE_FSMC_D6) |       \
                                     PIN_OSPEED_100M(GPIOE_FSMC_D7) |       \
                                     PIN_OSPEED_100M(GPIOE_FSMC_D8) |       \
                                     PIN_OSPEED_100M(GPIOE_FSMC_D9) |       \
                                     PIN_OSPEED_100M(GPIOE_FSMC_D10) |      \
                                     PIN_OSPEED_100M(GPIOE_FSMC_D11) |      \
                                     PIN_OSPEED_100M(GPIOE_FSMC_D12))
#define VAL_GPIOE_PUPDR             (PIN_PUPDR_FLOATING(GPIOE_FSMC_NBL0) |  \
                                     PIN_PUPDR_FLOATING(GPIOE_FSMC_NBL1) |  \
                                     PIN_PUPDR_FLOATING(GPIOE_FSMC_A23) |   \
                                     PIN_PUPDR_FLOATING(GPIOE_FSMC_A19) |   \
                                     PIN_PUPDR_FLOATING(GPIOE_FSMC_A20) |   \
                                     PIN_PUPDR_FLOATING(GPIOE_FSMC_A21) |   \
                                     PIN_PUPDR_FLOATING(GPIOE_FSMC_A22) |   \
                                     PIN_PUPDR_FLOATING(GPIOE_FSMC_D4) |    \
                                     PIN_PUPDR_FLOATING(GPIOE_FSMC_D5) |    \
                                     PIN_PUPDR_FLOATING(GPIOE_FSMC_D6) |    \
                                     PIN_PUPDR_FLOATING(GPIOE_FSMC_D7) |    \
                                     PIN_PUPDR_FLOATING(GPIOE_FSMC_D8) |    \
                                     PIN_PUPDR_FLOATING(GPIOE_FSMC_D9) |    \
                                     PIN_PUPDR_FLOATING(GPIOE_FSMC_D10) |   \
                                     PIN_PUPDR_FLOATING(GPIOE_FSMC_D11) |   \
                                     PIN_PUPDR_FLOATING(GPIOE_FSMC_D12))
#define VAL_GPIOE_ODR               (PIN_ODR_HIGH(GPIOE_FSMC_NBL0) |        \
                                     PIN_ODR_HIGH(GPIOE_FSMC_NBL1) |        \
                                     PIN_ODR_HIGH(GPIOE_FSMC_A23) |         \
                                     PIN_ODR_HIGH(GPIOE_FSMC_A19) |         \
                                     PIN_ODR_HIGH(GPIOE_FSMC_A20) |         \
                                     PIN_ODR_HIGH(GPIOE_FSMC_A21) |         \
                                     PIN_ODR_HIGH(GPIOE_FSMC_A22) |         \
                                     PIN_ODR_HIGH(GPIOE_FSMC_D4) |          \
                                     PIN_ODR_HIGH(GPIOE_FSMC_D5) |          \
                                     PIN_ODR_HIGH(GPIOE_FSMC_D6) |          \
                                     PIN_ODR_HIGH(GPIOE_FSMC_D7) |          \
                                     PIN_ODR_HIGH(GPIOE_FSMC_D8) |          \
                                     PIN_ODR_HIGH(GPIOE_FSMC_D9) |          \
                                     PIN_ODR_HIGH(GPIOE_FSMC_D10) |         \
                                     PIN_ODR_HIGH(GPIOE_FSMC_D11) |         \
                                     PIN_ODR_HIGH(GPIOE_FSMC_D12))
#define VAL_GPIOE_AFRL              (PIN_AFIO_AF(GPIOE_FSMC_NBL0, 12) |     \
                                     PIN_AFIO_AF(GPIOE_FSMC_NBL1, 12) |     \
                                     PIN_AFIO_AF(GPIOE_FSMC_A23, 12) |      \
                                     PIN_AFIO_AF(GPIOE_FSMC_A19, 12) |      \
                                     PIN_AFIO_AF(GPIOE_FSMC_A20, 12) |      \
                                     PIN_AFIO_AF(GPIOE_FSMC_A21, 12) |      \
                                     PIN_AFIO_AF(GPIOE_FSMC_A22, 12) |      \
                                     PIN_AFIO_AF(GPIOE_FSMC_D4, 12))
#define VAL_GPIOE_AFRH              (PIN_AFIO_AF(GPIOE_FSMC_D5, 12) |       \
                                     PIN_AFIO_AF(GPIOE_FSMC_D6, 12) |       \
                                     PIN_AFIO_AF(GPIOE_FSMC_D7, 12) |       \
                                     PIN_AFIO_AF(GPIOE_FSMC_D8, 12) |       \
                                     PIN_AFIO_AF(GPIOE_FSMC_D9, 12) |       \
                                     PIN_AFIO_AF(GPIOE_FSMC_D10, 12) |      \
                                     PIN_AFIO_AF(GPIOE_FSMC_D11, 12) |      \
                                     PIN_AFIO_AF(GPIOE_FSMC_D12, 12))

/*
 * GPIOF setup:
 *
 * PF0  - FSMC_A0                   (alternate 12).
 * PF1  - FSMC_A1                   (alternate 12).
 * PF2  - FSMC_A2                   (alternate 12).
 * PF3  - FSMC_A3                   (alternate 12).
 * PF4  - FSMC_A4                   (alternate 12).
 * PF5  - FSMC_A5                   (alternate 12).
 * PF6  - PIN6                      (input pullup).
 * PF7  - PIN7                      (input pullup).
 * PF8  - PIN8                      (input pullup).
 * PF9  - PIN9                      (input pullup).
 * PF10 - PIN10                     (input pullup).
 * PF11 - USB_FS_FAULT_OVER_CURRENT (input floating).
 * PF12 - FSMC_A6                   (alternate 12).
 * PF13 - FSMC_A7                   (alternate 12).
 * PF14 - FSMC_A8                   (alternate 12).
 * PF15 - FSMC_A9                   (alternate 12).
 */
#define VAL_GPIOF_MODER             (PIN_MODE_ALTERNATE(GPIOF_FSMC_A0) |    \
                                     PIN_MODE_ALTERNATE(GPIOF_FSMC_A1) |    \
                                     PIN_MODE_ALTERNATE(GPIOF_FSMC_A2) |    \
                                     PIN_MODE_ALTERNATE(GPIOF_FSMC_A3) |    \
                                     PIN_MODE_ALTERNATE(GPIOF_FSMC_A4) |    \
                                     PIN_MODE_ALTERNATE(GPIOF_FSMC_A5) |    \
                                     PIN_MODE_INPUT(GPIOF_PIN6) |           \
                                     PIN_MODE_INPUT(GPIOF_PIN7) |           \
                                     PIN_MODE_INPUT(GPIOF_PIN8) |           \
                                     PIN_MODE_INPUT(GPIOF_PIN9) |           \
                                     PIN_MODE_INPUT(GPIOF_PIN10) |          \
                                     PIN_MODE_INPUT(GPIOF_USB_FS_FAULT_OVER_CURRENT) |\
                                     PIN_MODE_ALTERNATE(GPIOF_FSMC_A6) |    \
                                     PIN_MODE_ALTERNATE(GPIOF_FSMC_A7) |    \
                                     PIN_MODE_ALTERNATE(GPIOF_FSMC_A8) |    \
                                     PIN_MODE_ALTERNATE(GPIOF_FSMC_A9))
#define VAL_GPIOF_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOF_FSMC_A0) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOF_FSMC_A1) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOF_FSMC_A2) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOF_FSMC_A3) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOF_FSMC_A4) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOF_FSMC_A5) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOF_PIN6) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOF_PIN7) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOF_PIN8) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOF_PIN9) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOF_PIN10) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOF_USB_FS_FAULT_OVER_CURRENT) |\
                                     PIN_OTYPE_PUSHPULL(GPIOF_FSMC_A6) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOF_FSMC_A7) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOF_FSMC_A8) |    \
                                     PIN_OTYPE_PUSHPULL(GPIOF_FSMC_A9))
#define VAL_GPIOF_OSPEEDR           (PIN_OSPEED_100M(GPIOF_FSMC_A0) |       \
                                     PIN_OSPEED_100M(GPIOF_FSMC_A1) |       \
                                     PIN_OSPEED_100M(GPIOF_FSMC_A2) |       \
                                     PIN_OSPEED_100M(GPIOF_FSMC_A3) |       \
                                     PIN_OSPEED_100M(GPIOF_FSMC_A4) |       \
                                     PIN_OSPEED_100M(GPIOF_FSMC_A5) |       \
                                     PIN_OSPEED_100M(GPIOF_PIN6) |          \
                                     PIN_OSPEED_100M(GPIOF_PIN7) |          \
                                     PIN_OSPEED_100M(GPIOF_PIN8) |          \
                                     PIN_OSPEED_100M(GPIOF_PIN9) |          \
                                     PIN_OSPEED_100M(GPIOF_PIN10) |         \
                                     PIN_OSPEED_100M(GPIOF_USB_FS_FAULT_OVER_CURRENT) |\
                                     PIN_OSPEED_100M(GPIOF_FSMC_A6) |       \
                                     PIN_OSPEED_100M(GPIOF_FSMC_A7) |       \
                                     PIN_OSPEED_100M(GPIOF_FSMC_A8) |       \
                                     PIN_OSPEED_100M(GPIOF_FSMC_A9))
#define VAL_GPIOF_PUPDR             (PIN_PUPDR_FLOATING(GPIOF_FSMC_A0) |    \
                                     PIN_PUPDR_FLOATING(GPIOF_FSMC_A1) |    \
                                     PIN_PUPDR_FLOATING(GPIOF_FSMC_A2) |    \
                                     PIN_PUPDR_FLOATING(GPIOF_FSMC_A3) |    \
                                     PIN_PUPDR_FLOATING(GPIOF_FSMC_A4) |    \
                                     PIN_PUPDR_FLOATING(GPIOF_FSMC_A5) |    \
                                     PIN_PUPDR_PULLUP(GPIOF_PIN6) |         \
                                     PIN_PUPDR_PULLUP(GPIOF_PIN7) |         \
                                     PIN_PUPDR_PULLUP(GPIOF_PIN8) |         \
                                     PIN_PUPDR_PULLUP(GPIOF_PIN9) |         \
                                     PIN_PUPDR_PULLUP(GPIOF_PIN10) |        \
                                     PIN_PUPDR_FLOATING(GPIOF_USB_FS_FAULT_OVER_CURRENT) |\
                                     PIN_PUPDR_FLOATING(GPIOF_FSMC_A6) |    \
                                     PIN_PUPDR_FLOATING(GPIOF_FSMC_A7) |    \
                                     PIN_PUPDR_FLOATING(GPIOF_FSMC_A8) |    \
                                     PIN_PUPDR_FLOATING(GPIOF_FSMC_A9))
#define VAL_GPIOF_ODR               (PIN_ODR_HIGH(GPIOF_FSMC_A0) |          \
                                     PIN_ODR_HIGH(GPIOF_FSMC_A1) |          \
                                     PIN_ODR_HIGH(GPIOF_FSMC_A2) |          \
                                     PIN_ODR_HIGH(GPIOF_FSMC_A3) |          \
                                     PIN_ODR_HIGH(GPIOF_FSMC_A4) |          \
                                     PIN_ODR_HIGH(GPIOF_FSMC_A5) |          \
                                     PIN_ODR_HIGH(GPIOF_PIN6) |             \
                                     PIN_ODR_HIGH(GPIOF_PIN7) |             \
                                     PIN_ODR_HIGH(GPIOF_PIN8) |             \
                                     PIN_ODR_HIGH(GPIOF_PIN9) |             \
                                     PIN_ODR_HIGH(GPIOF_PIN10) |            \
                                     PIN_ODR_HIGH(GPIOF_USB_FS_FAULT_OVER_CURRENT) |\
                                     PIN_ODR_HIGH(GPIOF_FSMC_A6) |          \
                                     PIN_ODR_HIGH(GPIOF_FSMC_A7) |          \
                                     PIN_ODR_HIGH(GPIOF_FSMC_A8) |          \
                                     PIN_ODR_HIGH(GPIOF_FSMC_A9))
#define VAL_GPIOF_AFRL              (PIN_AFIO_AF(GPIOF_FSMC_A0, 12) |       \
                                     PIN_AFIO_AF(GPIOF_FSMC_A1, 12) |       \
                                     PIN_AFIO_AF(GPIOF_FSMC_A2, 12) |       \
                                     PIN_AFIO_AF(GPIOF_FSMC_A3, 12) |       \
                                     PIN_AFIO_AF(GPIOF_FSMC_A4, 12) |       \
                                     PIN_AFIO_AF(GPIOF_FSMC_A5, 12) |       \
                                     PIN_AFIO_AF(GPIOF_PIN6, 0) |           \
                                     PIN_AFIO_AF(GPIOF_PIN7, 0))
#define VAL_GPIOF_AFRH              (PIN_AFIO_AF(GPIOF_PIN8, 0) |           \
                                     PIN_AFIO_AF(GPIOF_PIN9, 0) |           \
                                     PIN_AFIO_AF(GPIOF_PIN10, 0) |          \
                                     PIN_AFIO_AF(GPIOF_USB_FS_FAULT_OVER_CURRENT, 0) |\
                                     PIN_AFIO_AF(GPIOF_FSMC_A6, 12) |       \
                                     PIN_AFIO_AF(GPIOF_FSMC_A7, 12) |       \
                                     PIN_AFIO_AF(GPIOF_FSMC_A8, 12) |       \
                                     PIN_AFIO_AF(GPIOF_FSMC_A9, 12))

/*
 * GPIOG setup:
 *
 * PG0  - FSMC_A10                  (alternate 12).
 * PG1  - FSMC_A11                  (alternate 12).
 * PG2  - FSMC_A12                  (alternate 12).
 * PG3  - FSMC_A13                  (alternate 12).
 * PG4  - FSMC_A14                  (alternate 12).
 * PG5  - FSMC_A15                  (alternate 12).
 * PG6  - LED1                      (output pushpull maximum).
 * PG7  - PIN7                      (input pullup).
 * PG8  - LED2                      (output pushpull maximum).
 * PG9  - FSMC_NE2                  (alternate 12).
 * PG10 - PIN10                     (output pushpull maximum).
 * PG11 - ETH_RMII_TXEN             (alternate 11).
 * PG12 - PIN12                     (input pullup).
 * PG13 - ETH_RMII_TXD0             (alternate 11).
 * PG14 - ETH_RMII_TXD1             (alternate 11).
 * PG15 - BUTTON_USER               (input pullup).
 */
#define VAL_GPIOG_MODER             (PIN_MODE_ALTERNATE(GPIOG_FSMC_A10) |   \
                                     PIN_MODE_ALTERNATE(GPIOG_FSMC_A11) |   \
                                     PIN_MODE_ALTERNATE(GPIOG_FSMC_A12) |   \
                                     PIN_MODE_ALTERNATE(GPIOG_FSMC_A13) |   \
                                     PIN_MODE_ALTERNATE(GPIOG_FSMC_A14) |   \
                                     PIN_MODE_ALTERNATE(GPIOG_FSMC_A15) |   \
                                     PIN_MODE_OUTPUT(GPIOG_LED1) |          \
                                     PIN_MODE_INPUT(GPIOG_PIN7) |           \
                                     PIN_MODE_OUTPUT(GPIOG_LED2) |          \
                                     PIN_MODE_ALTERNATE(GPIOG_FSMC_NE2) |   \
                                     PIN_MODE_OUTPUT(GPIOG_PIN10) |         \
                                     PIN_MODE_ALTERNATE(GPIOG_ETH_RMII_TXEN) |\
                                     PIN_MODE_INPUT(GPIOG_PIN12) |          \
                                     PIN_MODE_ALTERNATE(GPIOG_ETH_RMII_TXD0) |\
                                     PIN_MODE_ALTERNATE(GPIOG_ETH_RMII_TXD1) |\
                                     PIN_MODE_INPUT(GPIOG_BUTTON_USER))
#define VAL_GPIOG_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOG_FSMC_A10) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOG_FSMC_A11) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOG_FSMC_A12) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOG_FSMC_A13) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOG_FSMC_A14) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOG_FSMC_A15) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOG_LED1) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOG_PIN7) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOG_LED2) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOG_FSMC_NE2) |   \
                                     PIN_OTYPE_PUSHPULL(GPIOG_PIN10) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOG_ETH_RMII_TXEN) |\
                                     PIN_OTYPE_PUSHPULL(GPIOG_PIN12) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOG_ETH_RMII_TXD0) |\
                                     PIN_OTYPE_PUSHPULL(GPIOG_ETH_RMII_TXD1) |\
                                     PIN_OTYPE_PUSHPULL(GPIOG_BUTTON_USER))
#define VAL_GPIOG_OSPEEDR           (PIN_OSPEED_100M(GPIOG_FSMC_A10) |      \
                                     PIN_OSPEED_100M(GPIOG_FSMC_A11) |      \
                                     PIN_OSPEED_100M(GPIOG_FSMC_A12) |      \
                                     PIN_OSPEED_100M(GPIOG_FSMC_A13) |      \
                                     PIN_OSPEED_100M(GPIOG_FSMC_A14) |      \
                                     PIN_OSPEED_100M(GPIOG_FSMC_A15) |      \
                                     PIN_OSPEED_100M(GPIOG_LED1) |          \
                                     PIN_OSPEED_100M(GPIOG_PIN7) |          \
                                     PIN_OSPEED_100M(GPIOG_LED2) |          \
                                     PIN_OSPEED_100M(GPIOG_FSMC_NE2) |      \
                                     PIN_OSPEED_100M(GPIOG_PIN10) |         \
                                     PIN_OSPEED_100M(GPIOG_ETH_RMII_TXEN) | \
                                     PIN_OSPEED_100M(GPIOG_PIN12) |         \
                                     PIN_OSPEED_100M(GPIOG_ETH_RMII_TXD0) | \
                                     PIN_OSPEED_100M(GPIOG_ETH_RMII_TXD1) | \
                                     PIN_OSPEED_100M(GPIOG_BUTTON_USER))
#define VAL_GPIOG_PUPDR             (PIN_PUPDR_FLOATING(GPIOG_FSMC_A10) |   \
                                     PIN_PUPDR_FLOATING(GPIOG_FSMC_A11) |   \
                                     PIN_PUPDR_FLOATING(GPIOG_FSMC_A12) |   \
                                     PIN_PUPDR_FLOATING(GPIOG_FSMC_A13) |   \
                                     PIN_PUPDR_FLOATING(GPIOG_FSMC_A14) |   \
                                     PIN_PUPDR_FLOATING(GPIOG_FSMC_A15) |   \
                                     PIN_PUPDR_PULLUP(GPIOG_LED1) |         \
                                     PIN_PUPDR_PULLUP(GPIOG_PIN7) |         \
                                     PIN_PUPDR_PULLDOWN(GPIOG_LED2) |       \
                                     PIN_PUPDR_FLOATING(GPIOG_FSMC_NE2) |   \
                                     PIN_PUPDR_FLOATING(GPIOG_PIN10) |      \
                                     PIN_PUPDR_FLOATING(GPIOG_ETH_RMII_TXEN) |\
                                     PIN_PUPDR_PULLUP(GPIOG_PIN12) |        \
                                     PIN_PUPDR_FLOATING(GPIOG_ETH_RMII_TXD0) |\
                                     PIN_PUPDR_FLOATING(GPIOG_ETH_RMII_TXD1) |\
                                     PIN_PUPDR_PULLUP(GPIOG_BUTTON_USER))
#define VAL_GPIOG_ODR               (PIN_ODR_HIGH(GPIOG_FSMC_A10) |         \
                                     PIN_ODR_HIGH(GPIOG_FSMC_A11) |         \
                                     PIN_ODR_HIGH(GPIOG_FSMC_A12) |         \
                                     PIN_ODR_HIGH(GPIOG_FSMC_A13) |         \
                                     PIN_ODR_HIGH(GPIOG_FSMC_A14) |         \
                                     PIN_ODR_HIGH(GPIOG_FSMC_A15) |         \
                                     PIN_ODR_LOW(GPIOG_LED1) |              \
                                     PIN_ODR_HIGH(GPIOG_PIN7) |             \
                                     PIN_ODR_LOW(GPIOG_LED2) |              \
                                     PIN_ODR_HIGH(GPIOG_FSMC_NE2) |         \
                                     PIN_ODR_HIGH(GPIOG_PIN10) |            \
                                     PIN_ODR_HIGH(GPIOG_ETH_RMII_TXEN) |    \
                                     PIN_ODR_HIGH(GPIOG_PIN12) |            \
                                     PIN_ODR_HIGH(GPIOG_ETH_RMII_TXD0) |    \
                                     PIN_ODR_HIGH(GPIOG_ETH_RMII_TXD1) |    \
                                     PIN_ODR_HIGH(GPIOG_BUTTON_USER))
#define VAL_GPIOG_AFRL              (PIN_AFIO_AF(GPIOG_FSMC_A10, 12) |      \
                                     PIN_AFIO_AF(GPIOG_FSMC_A11, 12) |      \
                                     PIN_AFIO_AF(GPIOG_FSMC_A12, 12) |      \
                                     PIN_AFIO_AF(GPIOG_FSMC_A13, 12) |      \
                                     PIN_AFIO_AF(GPIOG_FSMC_A14, 12) |      \
                                     PIN_AFIO_AF(GPIOG_FSMC_A15, 12) |      \
                                     PIN_AFIO_AF(GPIOG_LED1, 0) |           \
                                     PIN_AFIO_AF(GPIOG_PIN7, 0))
#define VAL_GPIOG_AFRH              (PIN_AFIO_AF(GPIOG_LED2, 0) |           \
                                     PIN_AFIO_AF(GPIOG_FSMC_NE2, 12) |      \
                                     PIN_AFIO_AF(GPIOG_PIN10, 0) |          \
                                     PIN_AFIO_AF(GPIOG_ETH_RMII_TXEN, 11) | \
                                     PIN_AFIO_AF(GPIOG_PIN12, 0) |          \
                                     PIN_AFIO_AF(GPIOG_ETH_RMII_TXD0, 11) | \
                                     PIN_AFIO_AF(GPIOG_ETH_RMII_TXD1, 11) | \
                                     PIN_AFIO_AF(GPIOG_BUTTON_USER, 0))

/*
 * GPIOH setup:
 *
 * PH0  - PIN0                      (input floating).
 * PH1  - PIN1                      (input floating).
 * PH2  - PIN2                      (input pullup).
 * PH3  - PIN3                      (input pullup).
 * PH4  - OTG_HS_ULPI_NXT           (alternate 10).
 * PH5  - OTG_FS_PowerSwitchON      (output pushpull maximum).
 * PH6  - PIN6                      (input pullup).
 * PH7  - PIN7                      (input pullup).
 * PH8  - PIN8                      (input pullup).
 * PH9  - PIN9                      (input pullup).
 * PH10 - PIN10                     (input pullup).
 * PH11 - PIN11                     (input pullup).
 * PH12 - PIN12                     (input pullup).
 * PH13 - SDIO_CARD_DETECT          (input floating).
 * PH14 - PIN14                     (input pullup).
 * PH15 - PIN15                     (input pullup).
 */
#define VAL_GPIOH_MODER             (PIN_MODE_INPUT(GPIOH_PIN0) |           \
                                     PIN_MODE_INPUT(GPIOH_PIN1) |           \
                                     PIN_MODE_INPUT(GPIOH_PIN2) |           \
                                     PIN_MODE_INPUT(GPIOH_PIN3) |           \
                                     PIN_MODE_ALTERNATE(GPIOH_OTG_HS_ULPI_NXT) |\
                                     PIN_MODE_OUTPUT(GPIOH_OTG_FS_PowerSwitchON) |\
                                     PIN_MODE_INPUT(GPIOH_PIN6) |           \
                                     PIN_MODE_INPUT(GPIOH_PIN7) |           \
                                     PIN_MODE_INPUT(GPIOH_PIN8) |           \
                                     PIN_MODE_INPUT(GPIOH_PIN9) |           \
                                     PIN_MODE_INPUT(GPIOH_PIN10) |          \
                                     PIN_MODE_INPUT(GPIOH_PIN11) |          \
                                     PIN_MODE_INPUT(GPIOH_PIN12) |          \
                                     PIN_MODE_INPUT(GPIOH_SDIO_CARD_DETECT) |\
                                     PIN_MODE_INPUT(GPIOH_PIN14) |          \
                                     PIN_MODE_INPUT(GPIOH_PIN15))
#define VAL_GPIOH_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOH_PIN0) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN1) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN2) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN3) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOH_OTG_HS_ULPI_NXT) |\
                                     PIN_OTYPE_PUSHPULL(GPIOH_OTG_FS_PowerSwitchON) |\
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN6) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN7) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN8) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN9) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN10) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN11) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN12) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOH_SDIO_CARD_DETECT) |\
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN14) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOH_PIN15))
#define VAL_GPIOH_OSPEEDR           (PIN_OSPEED_100M(GPIOH_PIN0) |          \
                                     PIN_OSPEED_100M(GPIOH_PIN1) |          \
                                     PIN_OSPEED_100M(GPIOH_PIN2) |          \
                                     PIN_OSPEED_100M(GPIOH_PIN3) |          \
                                     PIN_OSPEED_100M(GPIOH_OTG_HS_ULPI_NXT) |\
                                     PIN_OSPEED_100M(GPIOH_OTG_FS_PowerSwitchON) |\
                                     PIN_OSPEED_100M(GPIOH_PIN6) |          \
                                     PIN_OSPEED_100M(GPIOH_PIN7) |          \
                                     PIN_OSPEED_100M(GPIOH_PIN8) |          \
                                     PIN_OSPEED_100M(GPIOH_PIN9) |          \
                                     PIN_OSPEED_100M(GPIOH_PIN10) |         \
                                     PIN_OSPEED_100M(GPIOH_PIN11) |         \
                                     PIN_OSPEED_100M(GPIOH_PIN12) |         \
                                     PIN_OSPEED_100M(GPIOH_SDIO_CARD_DETECT) |\
                                     PIN_OSPEED_100M(GPIOH_PIN14) |         \
                                     PIN_OSPEED_100M(GPIOH_PIN15))
#define VAL_GPIOH_PUPDR             (PIN_PUPDR_FLOATING(GPIOH_PIN0) |       \
                                     PIN_PUPDR_FLOATING(GPIOH_PIN1) |       \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN2) |         \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN3) |         \
                                     PIN_PUPDR_FLOATING(GPIOH_OTG_HS_ULPI_NXT) |\
                                     PIN_PUPDR_PULLUP(GPIOH_OTG_FS_PowerSwitchON) |\
                                     PIN_PUPDR_PULLUP(GPIOH_PIN6) |         \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN7) |         \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN8) |         \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN9) |         \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN10) |        \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN11) |        \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN12) |        \
                                     PIN_PUPDR_FLOATING(GPIOH_SDIO_CARD_DETECT) |\
                                     PIN_PUPDR_PULLUP(GPIOH_PIN14) |        \
                                     PIN_PUPDR_PULLUP(GPIOH_PIN15))
#define VAL_GPIOH_ODR               (PIN_ODR_HIGH(GPIOH_PIN0) |             \
                                     PIN_ODR_HIGH(GPIOH_PIN1) |             \
                                     PIN_ODR_HIGH(GPIOH_PIN2) |             \
                                     PIN_ODR_HIGH(GPIOH_PIN3) |             \
                                     PIN_ODR_HIGH(GPIOH_OTG_HS_ULPI_NXT) |  \
                                     PIN_ODR_HIGH(GPIOH_OTG_FS_PowerSwitchON) |\
                                     PIN_ODR_HIGH(GPIOH_PIN6) |             \
                                     PIN_ODR_HIGH(GPIOH_PIN7) |             \
                                     PIN_ODR_HIGH(GPIOH_PIN8) |             \
                                     PIN_ODR_HIGH(GPIOH_PIN9) |             \
                                     PIN_ODR_HIGH(GPIOH_PIN10) |            \
                                     PIN_ODR_HIGH(GPIOH_PIN11) |            \
                                     PIN_ODR_HIGH(GPIOH_PIN12) |            \
                                     PIN_ODR_HIGH(GPIOH_SDIO_CARD_DETECT) | \
                                     PIN_ODR_HIGH(GPIOH_PIN14) |            \
                                     PIN_ODR_HIGH(GPIOH_PIN15))
#define VAL_GPIOH_AFRL              (PIN_AFIO_AF(GPIOH_PIN0, 0) |           \
                                     PIN_AFIO_AF(GPIOH_PIN1, 0) |           \
                                     PIN_AFIO_AF(GPIOH_PIN2, 0) |           \
                                     PIN_AFIO_AF(GPIOH_PIN3, 0) |           \
                                     PIN_AFIO_AF(GPIOH_OTG_HS_ULPI_NXT, 10) |\
                                     PIN_AFIO_AF(GPIOH_OTG_FS_PowerSwitchON, 0) |\
                                     PIN_AFIO_AF(GPIOH_PIN6, 0) |           \
                                     PIN_AFIO_AF(GPIOH_PIN7, 0))
#define VAL_GPIOH_AFRH              (PIN_AFIO_AF(GPIOH_PIN8, 0) |           \
                                     PIN_AFIO_AF(GPIOH_PIN9, 0) |           \
                                     PIN_AFIO_AF(GPIOH_PIN10, 0) |          \
                                     PIN_AFIO_AF(GPIOH_PIN11, 0) |          \
                                     PIN_AFIO_AF(GPIOH_PIN12, 0) |          \
                                     PIN_AFIO_AF(GPIOH_SDIO_CARD_DETECT, 0) |\
                                     PIN_AFIO_AF(GPIOH_PIN14, 0) |          \
                                     PIN_AFIO_AF(GPIOH_PIN15, 0))

/*
 * GPIOI setup:
 *
 * PI0  - PIN0                      (input pullup).
 * PI1  - PIN1                      (input pullup).
 * PI2  - PIN2                      (input pullup).
 * PI3  - PIN3                      (input pullup).
 * PI4  - PIN4                      (input pullup).
 * PI5  - PIN5                      (input pullup).
 * PI6  - PIN6                      (input pullup).
 * PI7  - PIN7                      (input pullup).
 * PI8  - PIN8                      (input pullup).
 * PI9  - LED3                      (output pushpull maximum).
 * PI10 - PIN10                     (input pullup).
 * PI11 - OTG_HS_ULPI_DIR           (alternate 10).
 * PI12 - PIN12                     (input pullup).
 * PI13 - PIN13                     (input pullup).
 * PI14 - PIN14                     (input pullup).
 * PI15 - PIN15                     (input pullup).
 */
#define VAL_GPIOI_MODER             (PIN_MODE_INPUT(GPIOI_PIN0) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN1) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN2) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN3) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN4) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN5) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN6) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN7) |           \
                                     PIN_MODE_INPUT(GPIOI_PIN8) |           \
                                     PIN_MODE_OUTPUT(GPIOI_LED3) |          \
                                     PIN_MODE_INPUT(GPIOI_PIN10) |          \
                                     PIN_MODE_ALTERNATE(GPIOI_OTG_HS_ULPI_DIR) |\
                                     PIN_MODE_INPUT(GPIOI_PIN12) |          \
                                     PIN_MODE_INPUT(GPIOI_PIN13) |          \
                                     PIN_MODE_INPUT(GPIOI_PIN14) |          \
                                     PIN_MODE_INPUT(GPIOI_PIN15))
#define VAL_GPIOI_OTYPER            (PIN_OTYPE_PUSHPULL(GPIOI_PIN0) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN1) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN2) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN3) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN4) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN5) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN6) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN7) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN8) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_LED3) |       \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN10) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOI_OTG_HS_ULPI_DIR) |\
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN12) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN13) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN14) |      \
                                     PIN_OTYPE_PUSHPULL(GPIOI_PIN15))
#define VAL_GPIOI_OSPEEDR           (PIN_OSPEED_100M(GPIOI_PIN0) |          \
                                     PIN_OSPEED_100M(GPIOI_PIN1) |          \
                                     PIN_OSPEED_100M(GPIOI_PIN2) |          \
                                     PIN_OSPEED_100M(GPIOI_PIN3) |          \
                                     PIN_OSPEED_100M(GPIOI_PIN4) |          \
                                     PIN_OSPEED_100M(GPIOI_PIN5) |          \
                                     PIN_OSPEED_100M(GPIOI_PIN6) |          \
                                     PIN_OSPEED_100M(GPIOI_PIN7) |          \
                                     PIN_OSPEED_100M(GPIOI_PIN8) |          \
                                     PIN_OSPEED_100M(GPIOI_LED3) |          \
                                     PIN_OSPEED_100M(GPIOI_PIN10) |         \
                                     PIN_OSPEED_100M(GPIOI_OTG_HS_ULPI_DIR) |\
                                     PIN_OSPEED_100M(GPIOI_PIN12) |         \
                                     PIN_OSPEED_100M(GPIOI_PIN13) |         \
                                     PIN_OSPEED_100M(GPIOI_PIN14) |         \
                                     PIN_OSPEED_100M(GPIOI_PIN15))
#define VAL_GPIOI_PUPDR             (PIN_PUPDR_PULLUP(GPIOI_PIN0) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN1) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN2) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN3) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN4) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN5) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN6) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN7) |         \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN8) |         \
                                     PIN_PUPDR_FLOATING(GPIOI_LED3) |       \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN10) |        \
                                     PIN_PUPDR_FLOATING(GPIOI_OTG_HS_ULPI_DIR) |\
                                     PIN_PUPDR_PULLUP(GPIOI_PIN12) |        \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN13) |        \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN14) |        \
                                     PIN_PUPDR_PULLUP(GPIOI_PIN15))
#define VAL_GPIOI_ODR               (PIN_ODR_HIGH(GPIOI_PIN0) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN1) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN2) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN3) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN4) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN5) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN6) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN7) |             \
                                     PIN_ODR_HIGH(GPIOI_PIN8) |             \
                                     PIN_ODR_LOW(GPIOI_LED3) |              \
                                     PIN_ODR_HIGH(GPIOI_PIN10) |            \
                                     PIN_ODR_HIGH(GPIOI_OTG_HS_ULPI_DIR) |  \
                                     PIN_ODR_HIGH(GPIOI_PIN12) |            \
                                     PIN_ODR_HIGH(GPIOI_PIN13) |            \
                                     PIN_ODR_HIGH(GPIOI_PIN14) |            \
                                     PIN_ODR_HIGH(GPIOI_PIN15))
#define VAL_GPIOI_AFRL              (PIN_AFIO_AF(GPIOI_PIN0, 0) |           \
                                     PIN_AFIO_AF(GPIOI_PIN1, 0) |           \
                                     PIN_AFIO_AF(GPIOI_PIN2, 0) |           \
                                     PIN_AFIO_AF(GPIOI_PIN3, 0) |           \
                                     PIN_AFIO_AF(GPIOI_PIN4, 0) |           \
                                     PIN_AFIO_AF(GPIOI_PIN5, 0) |           \
                                     PIN_AFIO_AF(GPIOI_PIN6, 0) |           \
                                     PIN_AFIO_AF(GPIOI_PIN7, 0))
#define VAL_GPIOI_AFRH              (PIN_AFIO_AF(GPIOI_PIN8, 0) |           \
                                     PIN_AFIO_AF(GPIOI_LED3, 0) |           \
                                     PIN_AFIO_AF(GPIOI_PIN10, 0) |          \
                                     PIN_AFIO_AF(GPIOI_OTG_HS_ULPI_DIR, 10) |\
                                     PIN_AFIO_AF(GPIOI_PIN12, 0) |          \
                                     PIN_AFIO_AF(GPIOI_PIN13, 0) |          \
                                     PIN_AFIO_AF(GPIOI_PIN14, 0) |          \
                                     PIN_AFIO_AF(GPIOI_PIN15, 0))


#if !defined(_FROM_ASM_)
#ifdef __cplusplus
extern "C" {
#endif
  void boardInit(void);
#ifdef __cplusplus
}
#endif
#endif /* _FROM_ASM_ */

#endif /* _BOARD_H_ */
